import React,{ useState, useEffect } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  TextInput,Button, Image, Dimensions
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import {bindActionCreators} from 'redux';
import * as reduxActions from '../redux/actions/actions';
import {connect} from 'react-redux';
import Loader from '../components/Loader';
import Toast from 'react-native-easy-toast';
import Modal from 'react-native-modal';
import {Feather} from "@expo/vector-icons";
import * as Permissions from 'expo-permissions';

const { height, width } = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
      },
      bubble: {
        position: 'relative',
        height: height * 0.75,
        width: height * 1.55,
        alignSelf: 'center',
        backgroundColor: '#5954C9',
        borderBottomLeftRadius: height * 0.8,
        borderBottomRightRadius: height * 0.8,
        overflow: 'visible'
      },
      buttonWraps: {
        alignSelf: 'center',
        position: 'absolute',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: width,
        bottom: -17,
        zIndex: 10,
        flexDirection: 'row'
      },
      button: {
        height: 40,
        minWidth: 110,
        borderWidth: 1,
        borderColor: '#c3c3c3',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        backgroundColor: '#fff'
      },
      buttonDark: {
        backgroundColor: '#2d2d2d',
        borderColor: 'transparent'
      },
      buttonTextDark: {
        color: '#fff'
      },
      buttonTextDisabled: {
        color: '#c3c3c3'
      },
      profileImage: {
        width: 133,
        height: 133,
        borderRadius: 133/2,
        marginTop:5,
        overflow: "hidden",
        borderWidth: 3,
        borderColor: "white"
    },
    add: {
      backgroundColor: "#41444B",
      position: "absolute",
      bottom: -10,
      left:51,
      width: 35,
      height: 35,
      borderRadius: 35/2,
      alignItems: "center",
      justifyContent: "center"
  }
})


class SignupScreen extends React.Component {
    
    state = {
        password: "",
        username: "",
        country: "",
        email: "",
        profileImage: "",
        setImage:"",
        image: null,
    };

    constructor(props) {
        super(props);
        console.log(props.reduxState.userdata);
    }
    
    ButtonClickCheckFunction = () => {
        this.props.reduxActions.signup(
            this.props.navigation,
            this.refs.toast,
            {
                username: this.state.username,
                password: this.state.password,
                country: this.state.country,
                email: this.state.email,
                profile_pick: this.state.profileImage,
                device_token: "XXXX",
                device_os: "XX",
            },
        );
    }
    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [4, 4],
          quality: 1,
        });
    
        console.log(result.uri);

        if (!result.cancelled) {
            setImage:result.uri;
          }
      };
    render() {
      let { image } = this.state;
        return (
            <View style={styles.container}>
        <View style={styles.bubble}>
        <View style={{marginLeft:"37.7%", marginTop:"5%"}}>
            <Text style={{fontSize: 27, color: 'white'}}>Sign Up</Text>
        </View>
        <View style={{marginLeft:"37.8%", marginTop:-25}}>
            <Text style={{fontSize: 26, color: 'white'}}>___</Text>
        </View>
        <View style={{ alignSelf: "center", marginTop:"1.5%", }}>
                    <View style={styles.profileImage}>
                        {image && <Image source={{ uri: image }} style={styles.image} resizeMode="center" />}
                    </View>
                    <View style={styles.add}>
                        <TouchableOpacity>
                            <Feather name="upload" size={20} color="#DFD8C8" onPress={this._pickImage}/>
                        </TouchableOpacity>
                    </View>
                </View>
        <View style={{marginLeft:"37.7%", marginTop:"2.5%"}}>
            <Text style={{fontSize: 16, color: 'white'}}>Username</Text>
            <TextInput style={{fontSize: 16, width:"39.4%",color: '#fff',borderBottomColor: '#ADD8E6', borderBottomWidth: 0.5,}}
                underlineColorAndroid = "transparent"
                autoCapitalize="none"
                type="text"
                onChangeText={username => this.setState({ username })} />
            <Text style={{fontSize: 16, color: 'white', marginTop:"1.5%"}}>Password</Text>
            <TextInput style={{fontSize: 16, width: '39.4%', color: '#fff', borderBottomColor: '#ADD8E6', borderBottomWidth: 0.5,}}
                secureTextEntry={true}
                underlineColorAndroid = "transparent"
                autoCapitalize="none"
                onChangeText={ password => this.setState({ password })} />
            <Text style={{fontSize: 16, color: 'white', marginTop:"1.5%"}}>Country</Text>
            <TextInput style={{fontSize: 16, width: '39.4%', color: '#fff', borderBottomColor: '#ADD8E6', borderBottomWidth: 0.5,}}
                underlineColorAndroid = "transparent"
                autoCapitalize="none"
                type="text"
                onChangeText={country => this.setState({ country })} />
            <Text style={{fontSize: 16, color: 'white', marginTop:"1.5%"}}>Email</Text>
            <TextInput style={{fontSize: 16, width: '39.4%', color: '#fff', borderBottomColor: '#ADD8E6', borderBottomWidth: 0.5,}}
                underlineColorAndroid = "transparent"
                autoCapitalize="none"
                type="email"
                onChangeText={email => this.setState({ email })} />
        </View>
          <View style={styles.buttonWraps}>
            <TouchableOpacity style={[styles.button, styles.buttonDark]} activeOpacity = {.7} onPress={ this.ButtonClickCheckFunction }>
              <Text style={styles.buttonTextDark}>Sign Up</Text>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity activeOpacity={.7} onPress={() => this.props.navigation.navigate('LoginScreen')}>
        <Text style={{fontSize: 16, width: '100%', color: '#a9b8c3', marginTop: 170,textAlign: 'center', paddingBottom: 20,}}>Already have an account?
        <Text style={{color: '#000',}}> LOGIN</Text></Text>
        </TouchableOpacity>
        <Toast ref="toast" style={{backgroundColor: 'black', justifyContent: 'center', width: '90%',}}
          position="bottom"
          positionValue={130}
          fadeInDuration={750}
          fadeOutDuration={2000}
          opacity={0.8}
          textStyle={{color: 'white', textAlign: 'center',fontSize: 18, fontWeight: 'bold',}}/>
                {
                    this.props.reduxState.loading
                    && <Loader />
                }
      </View>
        );
    }
    componentDidMount() {
      this.getPermissionAsync();
    }
  
    getPermissionAsync = async () => {
      if (Constants.platform.ios) {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    };
  
    _pickImage = async () => {
      try {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
        if (!result.cancelled) {
          this.setState({ image: result.uri });
        }
  
        console.log(result);
      } catch (E) {
        console.log(E);
      }
    };
}
const mapStateToProps = state => ({
  reduxState: state.reducers,
});

const mapDispatchToProps = dispatch => ({
  reduxActions: bindActionCreators(reduxActions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignupScreen);
