//Packages
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  ImageBackground,
  SafeAreaView,
  TextInput,
  Dimensions
} from 'react-native';
import Constants from 'expo-constants';

//Files
import {bindActionCreators} from 'redux';
import * as reduxActions from '../redux/actions/actions';
import {connect} from 'react-redux';
import Loader from '../components/Loader';
import Toast from 'react-native-easy-toast';

const { height, width } = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  bubble: {
    position: 'relative',
    height: height * 0.75,
    width: height * 1.75,
    alignSelf: 'center',
    backgroundColor: '#5954C9',
    borderBottomLeftRadius: height * 0.9,
    borderBottomRightRadius: height * 0.9,
    overflow: 'visible'
  },
  buttonWraps: {
    alignSelf: 'center',
    position: 'absolute',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: width,
    bottom: -17,
    zIndex: 10,
    flexDirection: 'row'
  },
  button: {
    height: 40,
    minWidth: 110,
    borderWidth: 1,
    borderColor: '#c3c3c3',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    backgroundColor: '#fff'
  },
  buttonDark: {
    backgroundColor: '#2d2d2d',
    borderColor: 'transparent'
  },
  buttonTextDark: {
    color: '#fff'
  },
  buttonTextDisabled: {
    color: '#c3c3c3'
  }
})


class LoginScreen extends React.Component {
    state = {
        userName: "redcrix1",
        password: "infantry",
    };

    constructor(props) {
        super(props);
        console.log(props.reduxState.userdata);
        if (props.reduxState.userdata && Object.keys(props.reduxState.userdata).length > 0) {
            props.navigation.navigate('HomeScreen');
        }
    }
  
    ButtonClickCheckFunction = () => {
        this.props.reduxActions.login(
            this.props.navigation,
            this.refs.toast,
            this.state.userName,
            this.state.password,
        );
    }

    render() {
        return (
        <View style={styles.container}>
        <View style={styles.bubble}>
        <View style={{marginLeft:"37.7%", marginTop:"10%"}}>
            <Text style={{fontSize: 27, color: 'white'}}>Login</Text>
        </View>
        <View style={{marginLeft:"37.8%", marginTop:-25}}>
            <Text style={{fontSize: 27, color: 'white'}}>___</Text>
        </View>
        <View style={{marginLeft:"37.7%", marginTop:"5%"}}>
            <Text style={{fontSize: 16, color: 'white'}}>Email</Text>
            <TextInput style={{fontSize: 16, width:"39.4%", marginTop:"1%",color: '#fff',borderBottomColor: '#ADD8E6', borderBottomWidth: 0.5,}}
                underlineColorAndroid = "transparent"
                autoCapitalize="none"
                type="email"
                value="redcrix1"
                onChangeText={userName => this.setState({ userName })} />
            <Text style={{fontSize: 16, color: 'white', marginTop:"3.3%"}}>Password</Text>
            <TextInput style={{fontSize: 16, width: '39.4%', color: '#fff', borderBottomColor: '#ADD8E6', borderBottomWidth: 0.5,}}
                secureTextEntry={true}
                underlineColorAndroid = "transparent"
                autoCapitalize="none"
                value="infantry"
                onChangeText={ password => this.setState({ password })} />
            <TouchableOpacity><Text style={{fontSize: 12, color: '#fff',marginTop: 9,marginLeft:"28.6%"}}>Forgot Password?</Text></TouchableOpacity>
        </View>
          <View style={styles.buttonWraps}>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('SignupScreen')} style={[styles.button]}>
              <Text style={styles.buttonTextDisabled}>SIGNUP</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.button, styles.buttonDark]} onPress={ this.ButtonClickCheckFunction }>
              <Text style={styles.buttonTextDark}>LOGIN</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Text style={{fontSize: 16, width: '100%', color: '#a9b8c3', marginTop: 170,textAlign: 'center', paddingBottom: 20,}}>Don't have an account?
        <Text style={{color: '#000',}}> SIGN UP</Text></Text>
        <Toast ref="toast" style={{backgroundColor: 'black', justifyContent: 'center', width: '90%',}}
            position="bottom"
            positionValue={130}
            fadeInDuration={750}
            fadeOutDuration={2000}
            opacity={0.8}
            textStyle={{color: 'white',textAlign: 'center', fontSize: 18, fontWeight: 'bold',}}
        />
                {
                    this.props.reduxState.loading
                    && <Loader />
                }
      </View>
        );
    }
}
const mapStateToProps = state => ({
  reduxState: state.reducers,
});

const mapDispatchToProps = dispatch => ({
  reduxActions: bindActionCreators(reduxActions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen);
