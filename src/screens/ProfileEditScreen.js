import React, {Component} from "react";
import { StyleSheet, Text, View, SafeAreaView, Image, TextInput, TouchableOpacity, } from "react-native";
import {Feather} from "@expo/vector-icons";
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

export default class ProfileScreen extends Component {
    state = {
        username: 'Redcrix1',
        password:'Sample',
        name: 'Redcrix',
        country: 'India',
        image: '../../assets/profile-pic.png',
    };
    handleUsername = (text) => {
        this.setState({ username: text })
     }
     handlePassword = (text) => {
        this.setState({ password: text })
     }
     handleName = (text) => {
        this.setState({ name: text })
     }
    closeDrawer(){
        this.drawer._root.close()
      };
    openDrawer(){
        this.drawer._root.open()
    };
    saveDetails = () => {
        alert('Profile Updated');
        this.props.navigation.navigate('ProfileScreen');
      };
    render(){
        let { image } = this.state;
    return (
        <SafeAreaView style={styles.container}>
            <View>
                <View style={styles.titleBar}>
                    <View>
                    <TouchableOpacity>
                        <Feather name="more-vertical" size={24} color="black" color="#5954C9" style={{ marginLeft:15}}/></TouchableOpacity>
                        </View>
                        <View >
                        <TouchableOpacity><Feather name="check" size={24} style={{ marginLeft:320}} color="#5954C9" onPress={this.saveDetails}/></TouchableOpacity>
                        </View>
                    
                </View>

                <View style={{ alignSelf: "center" }}>
                    <View style={styles.profileImage}>
                        {image && <Image source={{ uri: image }} style={styles.image} resizeMode="center" />}
                    </View>
                    <View style={styles.add}>
                        <TouchableOpacity>
                            <Feather name="upload" size={20} color="#DFD8C8" onPress={this._pickImage}/>
                        </TouchableOpacity>
                    </View>
                </View>
                
            </View>
            <View style={{ marginTop:20 }}>
            <Text style = {styles.fieldText}>Username</Text>
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = {this.state.username}
               placeholderTextColor = "#C4C4C4"
               autoCapitalize = "none"
               onChangeText = {this.handleUsername}
               autoFocus={true}/>
            <Text style = {[styles.fieldText,{marginTop:3}]}>Password</Text>
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = {this.state.password}
               placeholderTextColor = "#C4C4C4"
               autoCapitalize = "none"
               onChangeText = {this.handlePassword}
               autoFocus={true}/>
            <Text style = {[styles.fieldText,{marginTop:3}]}>Name</Text>
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = {this.state.name}
               placeholderTextColor = "#C4C4C4"
               autoCapitalize = "none"
               onChangeText = {this.handleName}
               autoFocus={true}/>
            </View>
        </SafeAreaView>
    );
    }
    componentDidMount() {
        this.getPermissionAsync();
      }
    
      getPermissionAsync = async () => {
        if (Constants.platform.ios) {
          const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
          if (status !== 'granted') {
            alert('Sorry, we need camera roll permissions to make this work!');
          }
        }
      };
    
      _pickImage = async () => {
        try {
          let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
          });
          if (!result.cancelled) {
            this.setState({ image: result.uri });
          }
    
          console.log(result);
        } catch (E) {
          console.log(E);
        }
      };
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    input: {
      margin: 15,
      height: 40,
      borderColor: '#7a42f4',
      borderWidth: 1,
      fontSize:18,
      paddingLeft:18,
      borderRadius:100,
      borderWidth:1.5 
   },
    text: {
        color: "#52575D"
    },
    image: {
        flex: 1,
        height: undefined,
        width: undefined
    },
    fieldText:{
        fontSize:16,
        marginLeft:20,
        marginBottom:-4
    },
    titleBar: {
        flexDirection: "row",
        marginTop: 50,
    },
    saveBar: {
        marginTop: -100,
        marginHorizontal: 100
    },
    profileImage: {
        width: 133,
        height: 133,
        borderRadius: 133/2,
        marginTop:5,
        overflow: "hidden",
        borderWidth: 3,
        borderColor: "#5954C8"
    },
    dm: {
        backgroundColor: "#41444B",
        position: "absolute",
        top: 15,
        width: 35,
        height: 35,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    add: {
        backgroundColor: "#41444B",
        position: "absolute",
        bottom: -10,
        left:51,
        width: 35,
        height: 35,
        borderRadius: 35/2,
        alignItems: "center",
        justifyContent: "center"
    }
});