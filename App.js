import {Provider} from 'react-redux';
import store from './src/redux/store';
import LoginScreen from './src/screens/LoginScreen';
import SignupScreen from './src/screens/SignupScreen';
import HomeScreen from './src/screens/HomeScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import SplashScreen from './src/screens/SplashScreen';
import ProfileEditScreen from './src/screens/ProfileEditScreen';
import * as React from 'react';

import { createDrawerNavigator,} from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();
function HomeStack() {
  return (
    <Provider store={store}>
    <Stack.Navigator>
      <Stack.Screen name="SplashScreen" options={{headerShown: false}} component={SplashScreen} />
      <Stack.Screen name="LoginScreen" options={{headerShown: false}} component={LoginScreen} />
      <Stack.Screen name="SignupScreen" options={{headerShown: false}} component={SignupScreen} />
      <Stack.Screen name="HomeScreen" options={{headerShown: false}} component={HomeScreen} />
      <Stack.Screen name="ProfileScreen" options={{headerShown: false}} component={ProfileScreen} />
      <Stack.Screen name="ProfileEditScreen" options={{headerShown: false}} component={ProfileEditScreen} />
    </Stack.Navigator>
    </Provider>
  );
}

const Drawer = createDrawerNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name=" Dashboard" component={HomeStack} />
        <Drawer.Screen name="Profile" component={ProfileScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
